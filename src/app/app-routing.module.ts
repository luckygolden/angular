import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { CodeComponent}  from './code/code.component';
import { LeaderboardComponent }  from './leaderboard/leaderboard.component';
import { SignupComponent }  from './signup/signup.component';
import { UserGifComponent }  from './user-gif/user-gif.component';


const routes: Routes = [
	{ path: '', redirectTo: '/dashboard', pathMatch: 'full'},
	{ path: 'dashboard', component: DashboardComponent},
	{ path: 'code', component: CodeComponent },
	{ path: 'leaderboard', component: LeaderboardComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'user-gif', component: UserGifComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}