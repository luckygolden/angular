import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { AppRoutingModule } from './/app-routing.module';
import { CodeComponent } from './code/code.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { SignupComponent } from './signup/signup.component';
import { UserGifComponent } from './user-gif/user-gif.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CodeComponent,
    LeaderboardComponent,
    SignupComponent,
    UserGifComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
